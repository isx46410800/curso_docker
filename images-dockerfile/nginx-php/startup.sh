#!/bin/bash

# Iniciar contenedor
echo "iniciando container..."

# Encendiendo servicio php y nginx
/usr/sbin/php-fpm -c /etc/php/fpm

nginx -g 'daemon off;'
